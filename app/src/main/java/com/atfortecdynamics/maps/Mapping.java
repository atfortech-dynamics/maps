package com.atfortecdynamics.maps;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

/**
 * Created by folio on 10/29/2018.
 */

public class Mapping extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    // below is the request code for allowing access of fine_location i.e GPS
    private final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1234;
    // the location variable is what will be used to store our current location
    private Location mLastKnownLocation;
    /* the fusedLocationProviderClient variable will be used to provide our last known location
        which will be used stored in our location variable
     */
    private FusedLocationProviderClient mFusedLocationProviderClient;
    //the DEFAULT_ZOOM variable will used to zoom our map to our location
    private int DEFAULT_ZOOM = 20;
    /* DEFAULT LATLONG variable will be used just in case our application failes to register our
        current location thus acts a fallback plan or plan B
     */
    private LatLng latLng = new LatLng(-1.28333, 36.81667);

    boolean mPermissionGranted;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        getLocationPermissions();
        getDeviceLocation();
    }

    /* we need to create a runtime permission request for our current
        location in order to use the gps/ location in phones with
        android version 6.0(API LEVEL 26) and above.

        inside your application you need to store a request code for the
        application. when a permission is granted, the request is sent back
        as a feedback to the application saying the application has been
        given permission
     */

    public void getLocationPermissions() {
        /* we will use this class to request for the access_course_location_permission
            ContextCompat requires 2 paramters i.e the Context/Activity requesting for the
            permission and the permission itself that we are requesting for

            when you use new String[] , it allows to ask for multiple permissions at the same time.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            // we will use th mPermissionGranted to check if we have the grantd permission to use the location
            mPermissionGranted = true;
        } else {
            /* this part means that the application has not been been granted any permission whatsoever
                thus we need to prompt the user to give us the permission afresh

                ActivityCompat class is what will be responsible for asking permission to our phnones
                it majorly requeires 3 things
                Activity -> where the location is being requested for
                String[] permissions - > all permission relevant to the activity are placed here
                    i.e we will request for permissions of fine_location and course_location
                 requestCode -> confirmation that we have been granted the permission
             */

            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION
            }, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

        }
    }

    /* we will use a method called  onRequestPermissionsResult to check whether the
        permissions we requested for were allowed or on
        if they were allowed we will proceed to use the GPS to access our current location

        it uses 3 paramaters
        requestCode - > to check if the requestCode was granted permission
        String[] permission - > check which permission were asked for
        int grantedResults - > to check which permission were granted the permission
     */

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int grantResults[]) {
        mPermissionGranted = false;

        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mPermissionGranted = true;
                }
            }

        }

        /* once the requests have been granted we proceed to applying our locations coordinate to
            our map using thr updateLocationUI method
         */

        updateLocationUI();
    }

    /* method updateLocationUI will add makers to our map and also our current location to the map

     */
    public void updateLocationUI() {
        /*check if the map exists
         */
        if (mMap == null) {
            return;
        }

        try {
            // check if we have our permissions granted
            if (mPermissionGranted) {
                // this will add the GPS button to our screen
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);

                //set our location to null to avoid the application crush
                mLastKnownLocation = null;
                // we will ask for the permission again
                getLocationPermissions();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* getDeviceLocation will be used to get our coordinates from the GPS signal
     */

    public void getDeviceLocation() {
        /*
              Get the best and most recent location of the device, which may be null in rare
                cases when a location is not available.
     */

        if (mPermissionGranted) {
            // we initiate a thread to check our stored last location from our gps signal
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            Task locationResult = mFusedLocationProviderClient.getLastLocation();

            /*we add a listener to our locationResult once it is complete by setting our location
                variable to the once stored in by the locationResult
                also use the same to add it to our map for viewing
             */

            locationResult.addOnCompleteListener(this, new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if(task.isSuccessful()){
                        // task was able to get the last known location of our device
                        mLastKnownLocation = (Location) task.getResult();

                        // let us get our current longitude and latitude from our lastKnownLocation

                        double latitude = mLastKnownLocation.getLatitude();
                        double longitude = mLastKnownLocation.getLongitude();

                        // creating a new LatLng object with our latitude and longitude

                        LatLng myCurrentLatLng = new LatLng(latitude,longitude);

                        //we pass our location to our map
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                // adding our myCurrentLatLng with our default zoom
                                myCurrentLatLng,DEFAULT_ZOOM
                        ));
                    }
                    else{
                        /* our task was not able to get our current location thus we will use our
                            default latlng variable that we set as defaults
                         */
                        Log.d("error","location is null");
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                latLng,DEFAULT_ZOOM
                        ));

                        mMap.getUiSettings().setMyLocationButtonEnabled(true);
                    }
                }
            });

        }
    }

}
